package com.example.minio.service;

import com.google.common.collect.Lists;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class MinioServiceTest {
    @Autowired
    private MinioService minioService;

    @Test
    public void test1() throws Exception {
        boolean aaa = minioService.bucketExists("test");
        System.out.println(aaa);

    }

    @Test
    void makeBucket() {
        boolean aaa = minioService.makeBucket("aaa");
        System.out.println(aaa);
    }

    @Test
    void listBucketNames() {
        List<String> strings = minioService.listBucketNames();
    }

    @Test
    void removeBucket() {
        System.out.println(minioService.removeBucket("test"));
    }


    @Test
    void getObjectUrl() {
        minioService.getObjectUrl("test","SquirrelSetup.log1");
    }

    @Test
    void removeObjectList() {
        minioService.removeObjectList("test", Lists.newArrayList("爬虫对应关系.txt","SquirrelSetup.log"));
    }
}