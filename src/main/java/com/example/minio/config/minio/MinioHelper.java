package com.example.minio.config.minio;

import io.minio.MinioClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioHelper {
    private final MinioConfig minioConfig;

    public MinioHelper(MinioConfig minioConfig) {
        this.minioConfig = minioConfig;
    }

    @Bean
    public MinioClient getMinioClient() {
        MinioClient minioClient = MinioClient.builder().endpoint(minioConfig.getEndpoint(), minioConfig.getPort(), minioConfig.getSecure()).credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey()).build();
        return minioClient;
    }
}
