package com.example.minio.controller;

import com.example.minio.service.MinioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@RestController
public class MinioController {
    @Autowired
    private MinioService minioService;

    @PostMapping("/upload")
    public void uploadFile(MultipartFile file) {
        minioService.putObject("test", file);
    }

    @GetMapping("/download")
    public void download(String bucketName, String fileName, HttpServletResponse response) {
        minioService.downloadFile(bucketName, fileName, response);
    }

    @GetMapping("/test")
    public String test(@RequestParam String a){
        return a;
    }
}
